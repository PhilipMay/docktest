from debian:bullseye-20211220-slim

LABEL maintainer="Philip May <eniak.info@gmail.com>"

RUN set -x && \
    apt-get update && \
    apt-get install -y nginx libnginx-mod-http-dav-ext
